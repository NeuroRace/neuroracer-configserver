#!/usr/bin/env python

"""
Given the following file structure:
  config/
   -default/
     -main.yaml
     -sub/
       -sub.yaml

And main.yaml:
example:
  my_number: 42

And sub/sub.yaml:
content:
  path: "arbitrarily_path"

A request could look like the following:

response = config_client.get(path="main", key="example")

The response would be a dictionary {"my_number" : 42}.

or:

response = config_client.get(path="sub/sub", key="content:path")

With the response "arbitrarily_path" as string.
"""

import rospy
from neuroracer_configserver.srv import GetConfig
import pickle

# Timeout for the client to connect to the server (in seconds)
SERVICE_CONNECTION_TIMEOUT = 5

# Service name of the config service
CONFIG_SERVICE_NAME = "/config_server/get_config"


class ConfigClient:
    def __init__(self):
        """
        Initializes a ConfigClient. Waits for the config service to come online and instantiates a ServiceProxy.
        """
        self.valid = True

        # setup service proxy
        try:
            rospy.wait_for_service(CONFIG_SERVICE_NAME, SERVICE_CONNECTION_TIMEOUT)

            # Maybe use <persistent=True> to increase performance
            self.config_service = rospy.ServiceProxy(CONFIG_SERVICE_NAME, GetConfig)
        except rospy.ROSException:
            self.valid = False
            raise rospy.ServiceException("couldn't connect to config_server at '%s'" % (CONFIG_SERVICE_NAME,))

    def get(self, path, key=""):
        """
        Performs a GetConfigRequest on the service_proxy and unpickles the data.

        :param path: The path to the file in the config base to get. Do not start with the config base path.
        :param key: The keys in the yaml file separated by ':'.
                    If omitted the complete file content will be returned represented as a dictionary.

        :returns: The element specified by "key" written in the file specified by "path". Can be a dictionary.

        :raises TypeError: Raises TypeError if key or path is not of type string.
        :raises ValueError: Raises ValueError if path is an empty String or if unpickling the response crashes.
        :raises IOError: Raises IOError if Server could not process the request properly.
        """
        if not self.valid:
            raise rospy.ServiceException("couldn't connect to config_server")

        if not isinstance(key, str):
            raise TypeError("Got key=\"%s\". Expected key of type <str>" % (key,))

        if not isinstance(path, str):
            raise TypeError("Got path=\"%s\". Expected key of type <str>", (path,))

        if path == "":
            raise ValueError("Got path=\"\".")

        # Try to get a response
        try:
            response = self.config_service(path, key)

            if response.valid:
                # Try to unpickle the response
                try:
                    value = pickle.loads(response.bytes)
                    return value
                except pickle.UnpicklingError as exc:
                    raise ValueError("Error during unpickling response (path=\"{}\", key=\"{}\". UnpicklingError: {}"
                                     .format(path, key, str(exc)))
            else:
                raise IOError("ConfigServer responded with Error:\n%s" % (response.bytes,))
        except rospy.ServiceException as exc:
            raise rospy.ServiceException("Service returned Error Message (key=\"%s\"):\n%s" % (key, str(exc)))
