# Config Server / Config Client
The Config Server is a ROS-Node that manages configurations stored in yaml-files.
These configurations can be requested with the Config Client.

A Configuration is a dictionary with keys of type string and
values of type int, float, bool, string or dict.

The goal is to replace local yaml files with a global configbase.

**NOTE:**  
This package is **not required for training models**.  
It is intended to support [neuroracer-robot](https://gitlab.com/NeuroRace/neuroracer-robot) / [neuroracer-sim](https://gitlab.com/NeuroRace/neuroracer-sim) with a generic python config handler for a ROS environment.  
Due to its standalone capability, it can be easily included into any ROS project.

## Content
- [Python Version](#python-version)
- [Setup](#setup)
  - [Config Server](#config-server)
  - [Config Client](#config-client)
  - [Config Example](#config-example)
- [Starting the Config Server](#starting-the-config-server)
- [Using the Config Client](#starting-the-config-server)

## Python Version
Due to [ROS](http://wiki.ros.org/Documentation) Python 2.7 is required. As soon as ROS fully supports Python3.x, the Python2.7 support will be dropped.

## Setup
Make sure you have ROS installed.

### Config Server
If you want to use the server for the neuroracer project you have to clone it into 
`src` directory of the catkin workspace of the [neuroracer-robot](https://gitlab.com/NeuroRace/neuroracer-robot) / [neuroracer-sim](https://gitlab.com/NeuroRace/neuroracer-sim).

### Config Client
To install the client:

```bash
cd neuroracer-configserver
python setup.py install --user
```

### Config Example
Given the following configbase:

```
configbase/
+-- main/
|   +-- a.yaml
+-- b.yaml
```

With main/a.yaml:

```yaml
image:
  width: 70
  height: 50
color: "rbg"
```

And b.yaml:

```yaml
person:
  name: "Julius"
  age: 33
```

## Starting the Config Server
Make sure roscore is running.

To start a config server:

```bash
rosrun neuroracer_configserver config_server.py
# or
rosrun neuroracer_configserver config_server.py "path/to/configbase"
```

You can select a directory in which all yaml files were read and available for the client.

## Using the Config Client
In a python script you can do the following:

```python
from neuroracer_configclient import ConfigClient

config_client = ConfigClient()
image_width = config_client.get(path="main/a", key="image:width") # image_width = 70
julius = config_client.get(path="b", key="person") # julius = {"name":"Julius", "age":33}

# You can omit the "key" argument to get the complete file:
a_yaml = config_client.get(path="main/a") # a_yaml = {"image": {"width":70, "height":50}, "color":"rgb"}
```
