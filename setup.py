from setuptools import setup, find_packages
from codecs import open  # To use a consistent encoding
from os import path

here = path.abspath(path.dirname(__file__))

long_description = 'Simple config client to query a configuration server'

setup(
    name='neuroracer_config',
    version='0.1.1',
    description='A simple configserver and a configclient to query the server',
    long_description=long_description,
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],
    keywords='ros configuration development',
    packages=find_packages(),
    install_requires=["PyYAML"],
)
