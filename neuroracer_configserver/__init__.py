from neuroracer_configserver.config_server import run

__all__ = [
    "run"
]
