#!/usr/bin/env python

"""
This file opens a config_server node (a ROS-Node).
You can set the config base path via the first command line argument.
The default for the config base is './config/default/'.
The config server will recursive scan every yaml file in the config base.
To see how to perform requests look under config_client.py.
"""

import sys
import rospy
from neuroracer_configserver.srv import GetConfig, GetConfigResponse
import yaml
import pickle
import os

config_data = {}
DEFAULT_CONFIG_BASE_PATH = "./configs/"
ELEMENT_SPLIT_SIGN = ":"


def _file_path_to_dict_key(path_to_file, path_to_base):
    """
    Converts a file path into a key for config_data, by normalizing
    the path, cutting away the path_to_base and removing file extension.

    :param path_to_file: The Path to the file to convert into a key.
    :param path_to_base: Needed to remove path_to_base from the file path.

    :returns: The resulting key
    """

    return os.path.splitext(os.path.normpath(path_to_file).replace(os.path.normpath(path_to_base) + "/", "", 1))[0]


def _add_yaml_file(path_to_file, path_to_base):
    """
    Scans a yaml file and adds it as value into config_data.
    The key will be the file name without .yaml ending and without config_base_path in the beginning.

    :param path_to_file: The Path to the file to convert add to config_data.
    :param path_to_base: The Path of the config base.

    :raises IOError: Raises IOError, if the given yaml file could not be parsed.
    """

    with open(path_to_file, 'r') as data:
        try:
            global config_data
            dict_key = _file_path_to_dict_key(path_to_file, path_to_base)
            config_data[dict_key] = yaml.load(data)
        except yaml.YAMLError as exc:
            raise IOError("Couldn't parse '{}'. yaml.load failed:\n{}".format(path_to_file, str(exc)))


def _get_all_files_in(path):
    """
    Walks through a directory and returns a list of all files in this directory.

    :param path: the directory to walk through.

    :returns: A list of all files in this directory.
    """

    files = []
    for (dir_path, _, file_names) in os.walk(path):
        for filename in file_names:
            files.append(os.path.join(dir_path, filename))

    return files


def _load_config_base(path_to_base):
    """
    Adds the content of all yaml files in path_to_base into config_data.

    :param path_to_base: The path to the config base.
    """

    yaml_files = filter(lambda filename: filename.endswith(".yaml"), _get_all_files_in(path_to_base))
    for yaml_file in yaml_files:
        _add_yaml_file(yaml_file, path_to_base)


# Responses

def _search_element(a_dict, key):
    """
    Gets a dictionary corresponding with a file and searches for the value of the keys in key.

    :param a_dict: The dictionary to search in.
    :param key: The list of keys separated by ELEMENT_SPLIT_SIGN.

    :returns: The value of the element if found otherwise None.
    """

    if key is "":
        return a_dict

    key_list = key.split(ELEMENT_SPLIT_SIGN)

    tmp_dict = a_dict
    for key in key_list:
        if key in tmp_dict:
            tmp_dict = tmp_dict[key]
        else:
            return None

    return tmp_dict


def _element_to_config_response(element):
    """
    Converts an element into a GetConfigResponse.

    :param element: A dictionary or a plain value to put in the response.

    :returns: The GetConfigResponse with the element in it.
    """

    return GetConfigResponse(pickle.dumps(element), True)


def _handle_get_element(req):
    """
    Handles a GetConfigRequest.

    :param req: The GetConfigRequest to handle.

    :returns: The GetConfigResponse with the element in it,
              if element found otherwise a GetConfigResponse with an error message.
    """

    dict_key = req.file
    if dict_key in config_data:
        element = _search_element(config_data[dict_key], req.key)
        if element is not None:
            return _element_to_config_response(element)
        else:
            return GetConfigResponse("Could not find key=\"" + req.key + "\" in \"" + dict_key + "\".", False)
            
    return GetConfigResponse("Could not find file=\"" + dict_key + ".yaml\".", False)


def run(config_base_path):
    """
    Opens a config server node and a config service "config_server/get_config".

    :param config_base_path: The path to the config_base directory
    :type config_base_path: str

    :raises IOError: Raises IOError if config_base_path could not be found.
    """

    # initialize ROS node and config-service
    rospy.init_node("config_server")
    rospy.Service("config_server/get_config", GetConfig, _handle_get_element)

    # load yaml files
    if os.path.isdir(config_base_path):
        _load_config_base(config_base_path)
    else:
        raise IOError("config base path '{}' not found".format(config_base_path))

    rospy.spin()


def main():
    try:
        config_base_path = DEFAULT_CONFIG_BASE_PATH
        if len(sys.argv) == 2:
            config_base_path = sys.argv[1]
        run(config_base_path)
    except rospy.ROSInterruptException:
        pass


if __name__ == "__main__":
    main()
