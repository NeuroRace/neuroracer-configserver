#!/usr/bin/env python

import rospy
from neuroracer_configclient import ConfigClient


def config_test():
	cc = ConfigClient()
	img = cc.get(path="default", key="image:width")
	print(img)


if __name__ == "__main__":
	try:
		config_test()
	except rospy.ROSInterruptException:
		pass
